<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * One product has many features. This is the inverse side.
     * @ORM\OneToMany(targetEntity="Noticia", mappedBy="autor", cascade={"persist", "remove"})
     */
    protected $treballador;

    public function __toString()
    {
        return (string) $this->getUsername();
    }

    public function __construct()
    {
        parent::__construct();
        $this->treballador= new ArrayCollection();
        // your own logic
    }



}