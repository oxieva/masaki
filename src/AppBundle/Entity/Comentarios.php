<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 04/01/19
 * Time: 11:21
 */

namespace AppBundle\Entity;


class Comentarios
{
    protected $comment;
    protected $dueDate;

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }


    public function getDueDate()
    {
        return $this->dueDate;
    }

    public function setDueDate(\DateTime $dueDate = null)
    {
        $this->dueDate = $dueDate;
    }
}