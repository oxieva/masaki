<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Seccion
 *
 * @ORM\Table(name="seccion")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SeccionRepository")
 */
class Seccion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * One product has many features. This is the inverse side.
     * @ORM\OneToMany(targetEntity="Noticia", mappedBy="seccion", cascade={"persist", "remove"})
     */
    protected $noticias;

    public function __construct() {
        $this->noticias= new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion.
     *
     * @param string $descripcion
     *
     * @return Seccion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion.
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function __toString()
    {
        return $this->getDescripcion();
    }

    /**
     * @return mixed
     * @param Noticia[] $noticias
     */
    public function getNoticias()
    {
        return $this->noticias;
    }

    /**
     * @param mixed $noticias
     */
    public function setNoticias($noticias)
    {
        $this->noticias -> clear();
        $this->noticias = new ArrayCollection();
    }
}
