<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PagesType
 *
 * @ORM\Table(name="pageContent")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PageContentRepository")
 */
class PageContent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * One Page has One MainNew Unidirectional.
     * @ORM\OneToOne(targetEntity="Noticia")
     * @ORM\JoinColumn(name="main_id", referencedColumnName="id")
     */
    private $mainNew;
    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=true)
     */
    private $titulo;
    /**
     * @var string
     *
     * @ORM\Column(name="body", type="string", length=2000, nullable=true)
     */
    private $body;

    /**
     * @ORM\ManyToMany(targetEntity="Noticia")
     * @ORM\JoinTable(name="page_news",
     *      joinColumns={@ORM\JoinColumn(name="page_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="new_id", referencedColumnName="id")}
     *      )
     */
    private $blockNews;
    /**
     * @var string
     *
     * @ORM\Column(name="labelText", type="string", length=255, nullable=true)
     */
    private $labelText;
    /**
     * @var string
     *
     * @ORM\Column(name="textArea", type="string", length=500, nullable=true)
     */
    private $textArea;
    /**
     * @var string
     *
     * @ORM\Column(name="buttonText", type="string", length=255, nullable=true)
     */
    private $buttonText;
    /**
     * @var string
     * @ORM\Column(name="buttonActionPage", type="string", length=255, nullable=true)
     */
    private $buttonActionPage;





    /**
     * Add a category in the product association.
     * (Owning side).
     *
     * @param $noticia Noticia the category to associate
     */
    public function addNoticia($noticia)
    {
        if ($this->blockNews->contains($noticia)) {
            return;
        }
        $this->blockNews->add($noticia);
    }
    /**
     * Remove a category in the product association.
     * (Owning side).
     *
     * @param $tag Palabrasclave the category to associate
     */
    public function removeNoticia($noticia)
    {
        if (!$this->blockNews->contains($noticia)) {
            return;
        }
        $this->blockNews->removeElement($noticia);
    }



    public function __construct()
    {
        $this->blockNews = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getMainNew()
    {
        return $this->mainNew;
    }

    /**
     * @param mixed $mainNew
     */
    public function setMainNew($mainNew)
    {
        $this->mainNew = $mainNew;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }



    /**
     * @return mixed
     * @param Noticia[] $blockNews
     */
    public function getBlockNews()
    {
        return $this->blockNews;
    }

    /**
     * @param mixed $blockNews
     */
    public function setBlockNews($blockNews)
    {
        $this->blockNews = $blockNews;
        $this->blockNews->clear();
        $this->blockNews = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * @return string
     */
    public function getLabelText()
    {
        return $this->labelText;
    }

    /**
     * @param string $labelText
     */
    public function setLabelText($labelText)
    {
        $this->labelText = $labelText;
    }

    /**
     * @return string
     */
    public function getTextArea()
    {
        return $this->textArea;
    }

    /**
     * @param string $textArea
     */
    public function setTextArea($textArea)
    {
        $this->textArea = $textArea;
    }

    /**
     * @return string
     */
    public function getButtonText()
    {
        return $this->buttonText;
    }

    /**
     * @param string $buttonText
     */
    public function setButtonText($buttonText)
    {
        $this->buttonText = $buttonText;
    }

    /**
     * @return string
     */
    public function getButtonActionPage()
    {
        return $this->buttonActionPage;
    }

    /**
     * @param string $buttonActionPage
     */
    public function setButtonActionPage($buttonActionPage)
    {
        $this->buttonActionPage = $buttonActionPage;
    }





}
