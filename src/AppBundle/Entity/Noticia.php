<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Noticia
 *
 * @ORM\Table(name="Noticia")
 * @ORM\Entity
 */
class Noticia
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=100, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitulo", type="string", length=200, nullable=false)
     */
    private $subtitulo;

    /**
     * Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="Seccion", inversedBy="noticias")
     * @ORM\JoinColumn(name="seccion_id", referencedColumnName="id", nullable=false)
     */
    protected $seccion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imagen", type="string", length=100, nullable=true)
     */
    private $imagen;

    /**
     * @var bool
     *
     * @ORM\Column(name="publicada", type="boolean", nullable=false)
     */
    private $publicada;

    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="string", length=2000, nullable=true)
     */
    private $texto;

    /**
     * Many features have one product. This is the owning side.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="username")
     * @ORM\JoinColumn(name="fos_user_id", referencedColumnName="id", nullable=false)
     */
    protected $autor;

    /**
     * List of categories where the post is
     * (Owning side).
     *
     * @var Palabrasclave[]
     * @ORM\ManyToMany(targetEntity="Palabrasclave", inversedBy="news", cascade={"persist"})
     * @ORM\JoinTable(name="noticias_palabrasClave",
     *      joinColumns={@ORM\JoinColumn(name="noticia_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="palabraClave_id", referencedColumnName="id")})
     */
    private $palabrasClave;

    public function __toString()
    {
        return $this->getTitulo();
    }

    public function __construct()
    {
        $this->palabrasClave = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return string
     */
    public function getSubtitulo()
    {
        return $this->subtitulo;
    }

    /**
     * @param string $subtitulo
     */
    public function setSubtitulo($subtitulo)
    {
        $this->subtitulo = $subtitulo;
    }



    /**
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param \DateTime $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return null|string
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * @param null|string $imagen
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    }

    /**
     * @return bool
     */
    public function isPublicada()
    {
        return $this->publicada;
    }

    /**
     * @param bool $publicada
     */
    public function setPublicada($publicada)
    {
        $this->publicada = $publicada;
    }


    /**
     * @return mixed
     */
    public function getSeccion()
    {
        return $this->seccion;
    }

    /**
     * @param mixed $seccion
     */
    public function setSeccion($seccion)
    {
        $this->seccion = $seccion;
    }

    /**
     * @return string
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * @param string $texto
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;
    }


    /**
     * @return mixed
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * @param mixed $autor
     */
    public function setAutor($autor)
    {
        $this->autor = $autor;
    }

    /**
     * @return Palabrasclave[]
     */
    public function getPalabrasClave()
    {
        return $this->palabrasClave;
    }

    /**
     * @param Palabrasclave[] $palabrasClave
     */
    public function setPalabrasClave($palabrasClave)
    {
        $this->palabrasClave = $palabrasClave;
        //throw new \RuntimeException(sprintf('exception X1X'));
        // This is the owning side, we have to call remove and add to have change in the category side too.
        /*foreach ($this->getPalabrasClave() as $palabraClave) {
            $this->removePalabraClave($palabraClave);
        }*/
        /*foreach ($palabrasClave as $palabraClave) {
            $this->addPalabraClave($palabraClave);
        }*/
    }

    /**
     * Add a category in the product association.
     * (Owning side).
     *
     * @param $palabraClave Palabrasclave the category to associate
     */
    public function addPalabraClave($palabraClave)
    {
        if ($this->palabrasClave->contains($palabraClave)) {
            return;
        }
        $this->palabrasClave->add($palabraClave);
        $palabraClave->addNoticia($this);
    }
    /**
     * Remove a category in the product association.
     * (Owning side).
     *
     * @param $tag Palabrasclave the category to associate
     */
    public function removePalabraClave($palabraClave)
    {
        if (!$this->palabrasClave->contains($palabraClave)) {
            return;
        }
        $this->palabrasClave->removeElement($palabraClave);
        $palabraClave->removeNoticia($this);
    }



}
