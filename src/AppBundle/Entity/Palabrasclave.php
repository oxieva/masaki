<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Palabrasclave
 *
 * @ORM\Table(name="PalabrasClave")
 * @ORM\Entity
 */
class Palabrasclave
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="string", length=45, nullable=false)
     */
    private $texto;


    /**
     * @ORM\ManyToMany(targetEntity="Noticia", mappedBy="palabrasClave", cascade={"persist"})
     */
    private $news;

    public function __toString()
    {
        return $this->getTexto();
    }

    public function __construct()
    {
        $this->news = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * @param string $texto
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * @param mixed $news
     */
    public function setNews($news)
    {
        $this->news = $news;
        $this->news = new ArrayCollection($news);
    }



    /**
     * Add a post in the category.
     *
     * @param $new Noticia The post to associate
     */

    public function addNoticia($new)
    {
        if ($this->news->contains($new)) {
            return;
        }

        $this->news->add($new);
        $new->addPalabraClave($this);
    }
    /**
     * @param Noticia $new
     */

    public function removeNoticia($new)
    {
        if (!$this->news->contains($new)) {
            return;
        }
        $this->news->removeElement($new);
        $new->removePalabraClave($this);
    }


}
