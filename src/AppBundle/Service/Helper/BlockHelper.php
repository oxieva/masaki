<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 27/12/18
 * Time: 09:42
 */

namespace AppBundle\Service\Helper;

use AppBundle\Entity\Noticia;
use AppBundle\Entity\PageContent;
use AppBundle\Entity\Palabrasclave;
use AppBundle\Entity\Seccion;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class BlockHelper
{

    private $entityManager;

    private $router;

    public function __construct(
        EntityManager $entityManager,
        UrlGeneratorInterface $router
    )
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function getMainMenuTree(){

        $menuTabs = [];
        $dropDown = [];
        $pageRepository = $this->entityManager->getRepository(PageContent::class);
        $pages = $pageRepository->findAll();


        foreach($pages as $page){
            $menuTabs[$page->getId()] = [
                'title' => $page->getTitle(),
                'link' => $this->router->generate('showPage',['title'=>$page->getTitle()])
            ];
        }

        $sectionRepository = $this->entityManager->getRepository(Seccion::class);
        $sections = $sectionRepository->findAll();


        $dropDown['title_dropdown'] = 'Secciones';
        $dropDown['children'] =[];
        foreach($sections as $section){

            $sectionItem = [

                'descripcion' => $section->getDescripcion(),

                'link' => $this->router->generate('seccionNoticias',['seccion'=>$section->getDescripcion()])
            ];

            $dropDown['children'][$section->getId()] = $sectionItem;
        }

        //dump($dropDown); exit();
        return [$menuTabs,$dropDown];
    }


    public function getKeywordsNewsTree(){
        $paraules = [];
        $titolNews =[];

        $paraulesRepo = $this->entityManager->getRepository(Palabrasclave::class);
        $paraulaClau = $paraulesRepo->findAll();

        foreach($paraulaClau as $page){
            $paraules[$page->getId()] = [
                'id' =>$page->getId(),
                'texto' => $page->getTexto(),

            ];
        }

        //dump($paraules); exit();

        $noticiesRepo = $this->entityManager->getRepository(Noticia::class);
        $titolsNews = $noticiesRepo->findAll();

        $titolNews['title_llista'] = 'Keywords';
        $titolsNews['children'] =[];
        foreach($titolsNews as $section){

            $sectionItem = [

                'titulo' => $section->getTitulo(),

                'subtitulo' =>$section->getSubtitulo(),
            ];

            $titolsNews['children'][$section->getId()] = $sectionItem;
        }
        dump($titolsNews); exit();
        return [$paraules, $titolsNews];
    }
    /*        {'menuTabs':[
                ['idTab':[
                    'title':valor_del_title,
                    'link': valor_del_link
            ]],
                ['idTab':[
                'title':valor_del_title,
                    'link': valor_del_link
            ]],
                ['idTab':[
                'title':valor_del_title,
                    'link': valor_del_link
            ]]
            }
            {'dropdownSeccions':
                'title_dropdown': 'valor_del title',
                ['children':
                    ['idTab':[
                        'title':valor_del_title,
                        'link': valor_del_link
            }
            ]*/


}