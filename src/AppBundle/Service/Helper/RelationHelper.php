<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 17/12/18
 * Time: 12:14
 */

namespace AppBundle\Service\Helper;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Noticia;
use AppBundle\Entity\Seccion;
use FOS\UserBundle\FOSUserBundle;

class RelationHelper
{

    private $entityManager;

    private $twig;

    public function __construct(EntityManager $entityManager /*\Twig_Environment $twig*/)
    {
        $this->entityManager = $entityManager;
        //$this->twig = $twig;
    }

    public function sayHi(){
        return 'hi';
    }

    public function createSectionAndFlush($description = 'dimsa'){
        /** @var $seccion*/
        $seccion = new Seccion();
        $seccion->setDescripcion($description);
        $this->entityManager->persist($seccion);
        $this->entityManager->flush();

        //dump($seccion->getId()); exit();

        //dump($seccion); exit();
        return $seccion;
    }


    public function showSeccionAction($seccion = 1){

        $seccion = new Seccion();

        //$seccion->getId();

        /*$seccion =$this -> entityManager->find('AppBundle:Seccion',$seccion);*/

        $seccion =$this -> entityManager->getRepository(Seccion::class)->findAll();

      //dump($seccion); exit();
        echo $seccion->getId() . $seccion->getDescripcion();

        //var_dump($seccion); exit();
        return $seccion;


    }

    public function createNewAndFlush($seccion){

        //dump($seccion); exit();
        /** @var  $noticia */

        $noticia = new Noticia();
        $noticia->getId();
        $noticia->setTitulo('title');


        $noticia->setUsername('prusti');
        $noticia->setSubtitulo('subtitle');
        $noticia->setFecha(new \DateTime());
        $noticia->setPublicada(1);
        $noticia->setSeccion($seccion);
        //dump($seccion); exit();


        $this->entityManager->persist($noticia);
        $this->entityManager->flush();

        //dump($noticia); exit();
        return $noticia;
    }


    public function createNewAndFlushWithSectionAndWorker($seccion, $treballador){

        //dump($seccion); exit();
        /** @var  $noticia */

        $noticia = new Noticia();
        $noticia->getId();
        $noticia->setTitulo('title');


        $noticia->setUsername($treballador);
        $noticia->setSubtitulo('subtitle');
        $noticia->setFecha(new \DateTime());
        $noticia->setPublicada(1);
        $noticia->setSeccion($seccion);
        //dump($seccion); exit();


        $this->entityManager->persist($noticia);
        $this->entityManager->flush();

        //dump($noticia); exit();
        return $noticia;
    }

}