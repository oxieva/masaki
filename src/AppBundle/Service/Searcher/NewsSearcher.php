<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 23/12/18
 * Time: 10:29
 */
namespace AppBundle\Service\Searcher;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Noticia;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class NewsSearcher
{
    private $entityManager;

    private $userSearcher;

    public function __construct(EntityManager $entityManager, UserSearcher $userSearcher)
    {
        $this->entityManager = $entityManager;
        $this->userSearcher = $userSearcher;
    }

    public function getHappyMessage()
    {

        return 'Great work! Keep going!';
    }

    public function showsNewsBySection($seccion = false, $page=1){

        $noticias = [];
        /*if($seccion){
            $repository = $this->entityManager->getRepository(Noticia::class);

            $noticias = $repository->findBy(
                array('seccion' => $seccion),
                array('id' => 'DESC'),
                $limit
            );

        }*/
        /*Contruir la query*/
        $repository = $this->entityManager->getRepository(Noticia::class);
        $queryBuilder= $repository->createQueryBuilder('n');

        if($seccion){
            /*$queryBuilder->select('n');
            $queryBuilder->from('Noticia', 'n');*/
            $queryBuilder->where ('n.seccion = :seccion');
            $queryBuilder->setParameter('seccion', $seccion);
            //dump($queryBuilder); exit();
            /*$queryBuilder->innerJoin('n.seccion', 's');
            $queryBuilder->andWhere('s.descripcion = :seccion');
            $queryBuilder-> setParameter('seccion', $seccion);*/
        }

        $q =$queryBuilder->getQuery();
        /*Preparem l'adaptador de la paginació*/
        //dump($q); exit();

        $adapter = new DoctrineORMAdapter($q);
        $pagerfanta = new Pagerfanta($adapter);
        /*limit (de moment hardcoded @todo: pass it thru config)*/
        $pagerfanta->setMaxPerPage(2);
        $pagerfanta->setCurrentPage($page);
        $nbResults = $pagerfanta->getNbResults();
        $noticias = $pagerfanta->getCurrentPageResults();

        return [$pagerfanta,$noticias];
    }

    public function selectNewsBy($paramArray = [], $page = 5){
        $noticias = [];
        $searchText = false;

        if(array_key_exists('simple_search', $paramArray)){
            $searchText = $paramArray['simple_search']['s'];
        }
        /*Recollim param q li passem des del controller*/

            //dump($paramArray); exit();

        /*Contruir la part comuna de la query*/
        $repository = $this->entityManager->getRepository(Noticia::class);
        $queryBuilder= $repository->createQueryBuilder('n');


        /*Discriminar i construir parts independents*/

        $paramHasAuthor = array_key_exists('autor', $paramArray);
        if($paramHasAuthor && strlen($paramArray['autor']) > 0){
            $autorName = $paramArray['autor'];
            $autor = $this->userSearcher->selectAuthorByUsername($autorName);

            //Si té autor:

            $queryBuilder-> where ('n.autor = :autor');
            $queryBuilder-> setParameter('autor', $autor);
        }

        // si busquem un text
        if($searchText){
            $queryBuilder->andWhere('n.texto LIKE :s');
            $queryBuilder->orWhere('n.titulo LIKE :s');
            $queryBuilder->setParameter('s', '%'.$searchText.'%');
        }

        //Si té paraula clau:
        $paramHasKeyword = array_key_exists('keyword', $paramArray);
        if($paramHasKeyword && strlen($paramArray['keyword']) > 0){
            $paraulaClau = $paramArray['keyword'];

            $queryBuilder->innerJoin('n.palabrasClave', 'p');
            $queryBuilder->andWhere('p.texto = :palabraClave');
            $queryBuilder->setParameter('palabraClave', $paraulaClau);
           //dump($paraulaClau); exit();
        }
// dump($queryBuilder); exit();
        /*Preparem l'adaptador de la paginació*/
        $adapter = new DoctrineORMAdapter($queryBuilder->getQuery());
        $pagerfanta = new Pagerfanta($adapter);
        /*limit (de moment hardcoded @todo: pass it thru config)*/
        $pagerfanta->setMaxPerPage(4);
        $pagerfanta->setCurrentPage($page);
        $nbResults = $pagerfanta->getNbResults();
        $noticias = $pagerfanta->getCurrentPageResults();

        /*Executar la query*/

        //$query = $queryBuilder->getQuery();

       // $noticias = $query->getResult();


        return [$pagerfanta,$noticias];
    }

    public function showsNewsByAuthor($autor = false){
        $noticias = [];
        if($autor){
            $repository = $this->entityManager->getRepository(Noticia::class);

            $noticias = $repository->findBy(
                array('autor' => $autor),
                array('id' => 'DESC')
            );

            dump($noticias);
        }

        return $noticias;
    }

    /*Aquí hauré de fer una cerca paraulesclau_noticies*/
    public function showsNewsByKeyword($keyword = false){
        $noticias = [];
        if($keyword){
            $repository = $this->entityManager->getRepository(Noticia::class);

            //dump($keyword->getId()); exit();
            $queryBulider= $repository->createQueryBuilder('n')
                ->innerJoin('n.palabrasClave', 'p');
            $queryBulider->where('p.id = :palabraClave');
            $queryBulider->setParameter('palabraClave', $keyword->getId());
           $query = $queryBulider->getQuery();

            //dump($query->getResult()); exit();
            $noticias= $query->getResult();

        }

        return $noticias;
    }
}