<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 27/12/18
 * Time: 09:42
 */

namespace AppBundle\Service\Searcher;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\User;
class UserSearcher
{

    private $entityManager;


    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;

    }

    public function selectAuthorByUsername($username = null){
        if($username){
            $repository = $this->entityManager->getRepository(User::class);

            $user = $repository->findOneBy(
                array('username' => $username)
            );
        }

        return $user;
        //dump($username); exit();
    }
}