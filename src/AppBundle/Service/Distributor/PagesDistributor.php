<?php

namespace AppBundle\Service\Distributor;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\PageContent;


class PagesDistributor
{
    private $entityManager;


    public function __construct(EntityManager $entityManager )
    {
        $this->entityManager = $entityManager;
        //$this->twig = $twig;
    }

    /*Metode Get data*/


    public function getDataPage($title ){
        $pageData = [];
        if($title){
            $pagina = $this->entityManager->getRepository('AppBundle:PageContent')->findOneBy(array(
                    'title' => $title
                )
            );

            $pageData['mainNew'] = $pagina->getMainNew();
            $pageData['blockNews'] = $pagina->getBlockNews();
            $pageData['titulo'] = $pagina->getTitulo();
            $pageData['body'] = $pagina->getBody();
            $pageData['labelText'] = $pagina->getLabelText();
            $pageData['textArea'] = $pagina->getTextArea();
            $pageData['buttonText'] = $pagina->getButtonText();
            $pageData['buttonActionPage'] = $pagina->getButtonActionPage();


           // dump($mainNew); exit();

        }


        return $pageData;
    }
}