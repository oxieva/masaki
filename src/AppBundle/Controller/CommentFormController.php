<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 04/01/19
 * Time: 11:24
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Comentarios;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class CommentFormController extends Controller
{


    public function newCommentAction(Request $request)
    {
        // creates a task and gives it some dummy data for this example
        $task = new Comentarios();
        $task->setComment('Write a comment');
        $task->setDueDate(new \DateTime('tomorrow'));

        $form = $this->createFormBuilder($task)
            ->add('comment', TextType::class)
            ->add('dueDate', DateType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Comment'))
            ->getForm();

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            // $entityManager = $this->getDoctrine()->getManager();
            // $entityManager->persist($task);
            // $entityManager->flush();

            return $this->redirectToRoute('task_success');
        }

        return $this->render('AppBundle:components:form.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}