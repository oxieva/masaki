<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 27/12/18
 * Time: 13:21
 */

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class OtherPagesController extends Controller
{

    /**
     * @Route("/pages", name="otherPages")
     *
     */
    public function otherPages(){

    }
}