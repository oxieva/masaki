<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 18/12/18
 * Time: 11:46
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Noticia;
use AppBundle\Entity\Palabrasclave;
use AppBundle\Entity\Seccion;
use AppBundle\Service\Helper\RelationHelper;
use AppBundle\Service\Searcher\NewsSearcher;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class NoticiaController extends Controller
{
    /**
     * @Route("/noticias/{id}", name="showNoticia", methods={"GET","HEAD"}, requirements={"id"="\d+"})
     */
    public function showNoticiaAction($id){

        $noticia = $this->getDoctrine()->getRepository('AppBundle:Noticia')->find($id);
        if(!$noticia) {
            throw $this->createNotFoundException(
                'No he trobat la new amb id: '.$id
            );
        }

        dump($noticia); exit();
        $noticiaTitol = $noticia->getTitulo();
        return  new Response('El titol de la noticia ' . $noticia->getId() . ' és ' . $noticiaTitol);

    }
    /**
     * @Route("/noticias/{seccion}", name="seccionNoticias")
     * @ParamConverter("seccion", options={"mapping": {"seccion": "descripcion"}})
     */

    public function showAllNewsBySection(RequestStack $requestStack,Seccion $seccion){
        $newsSearcher = $this->get('app.news.searcher');
        $request = $requestStack->getCurrentRequest();
        $page = $request->get('page');
        if($page != null){
            $pageIndex = $page;
        }else{
            $pageIndex = 1;
        }

        list($pager, $noticias)= $newsSearcher->showsNewsBySection($seccion, $pageIndex);


        $currentRequest = $requestStack->getCurrentRequest();

        /*return new Response('El servei es carrega');*/
        return $this->render('AppBundle:full:sectionContent.html.twig', array(
            'pager' => $pager,
            'noticias' => $noticias,
            'seccion' => $seccion

        ));

    }
    

    /**
     * @Route("/create", name="crearNoticia")
     */
    public function createNoticiaAction()
    {

        //dump('Hola1era'); exit();
        //$seccion->setDescripcion('Actualitat');

        /**@var RelationHelper $relationHelper*/
        $relationHelper = $this->get('app.relation.test.helper');

        $seccion = $relationHelper->createSectionAndFlush();
        /*$noticia = $relationHelper->createNewAndFlush($seccion);*/

        $em = $this->getDoctrine()->getManager();

        $keyword = new Palabrasclave();

        //$keyword ->setId(1);
        //$keyword ->getTexto();
        $keyword->setTexto('Desembre');


        //$em->persist($keyword);
       // $em->flush();
        //dump($keyword); exit();

        $noticia = new Noticia();
        $noticia->setTitulo('Super new');
        $noticia->setUsername($this->getUser());
        //dump($this->getUser()); exit();
        $noticia->setAutor($this->getUser());

        $noticia->setSubtitulo('subtitle de dimecres, abans dinar');
        $noticia->setFecha(new \DateTime('now'));
        $noticia->setPublicada(1);

        $noticia->setSeccion($seccion);

        //var_dump($keyword); exit();
        $noticia->setPalabrasClave([$keyword]);

        $em = $this->getDoctrine()->getManager();
        $em->persist($seccion);
        $em->persist($noticia);
        $em->flush();


        return new Response('Hem creat la new '.$noticia->getId().
            ' de la secció '.$seccion->getDescripcion() . ' de la secció ' . $keyword->getTexto() );
    }

}