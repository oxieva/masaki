<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\PageContent;

class PageController extends Controller
{

    /**
     * @Route("/page/{title}", name="showPage", methods={"GET","HEAD","POST"})
     */
    public function showGenericPageAction($title = null){

        $getDataPage = $this->get('app.pages.distributor');

        $pageData = $getDataPage->getDataPage($title);

        //dump($createPage); exit();

        //$ppalNew = $pagina->getMainNew();

        /*$newsBlock = $pagina->getBlockNews();
        dump($newsBlock); exit();*/

        return $this->render('AppBundle:full:pageContent.html.twig', array(
            'pageData' => $pageData,
            'mainNew' => $pageData['mainNew'],
            'blockNews' => $pageData['blockNews'],

        ));

    }


}