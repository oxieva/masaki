<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Noticia;
use AppBundle\Entity\Seccion;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Service\Helper\RelationHelper;

class DefaultController extends Controller

{
    protected $twig;
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('AppBundle:components:slider.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/test", name="test")
     */
    public function testAction(Request $request)
    {
        /*$seccion = new Seccion();
        $seccion->getId();
        $seccion->setDescripcion('Symfony');*/

        /**@var \AppBundle\Service\Helper\RelationHelper $helperRelacions */
        $helperRelacions = $this->get('app.relation.test.helper');
        dump($helperRelacions->showSeccionAction()); exit();
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => 'this is a test',
        ]);
    }


}
