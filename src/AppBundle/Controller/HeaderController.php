<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 02/01/19
 * Time: 12:53
 */

namespace AppBundle\Controller;


use AppBundle\Service\Helper\BlockHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Seccion;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Form\SimpleSearchType;

class HeaderController extends Controller
{

    public function headerAction()
    {
        /*Necessito titul i enllaç d les pagines q sortiran al menu*/
        /**@var BlockHelper $blockHelper */


        $blockHelper = $this->get('app.block_helper');
       // $searchFormType = $this->get('app.simple_search_form');
        $searchForm = $this->createForm(SimpleSearchType::class)->createView();


        /*$buscadorKeywords = $blockHelper->getKeywordsNewsTree();
        dump($buscadorKeywords); exit();*/
        list($menuTabs, $dropDown) = $blockHelper->getMainMenuTree();

        /*dump($menuTabs, $dropDown);

        exit();*/

        return $this->render('AppBundle:components:header.html.twig', array(
            'menuTabs' => $menuTabs,
            'dropDown' => $dropDown,
            'simpleSearchForm' => $searchForm
        ));


    }




}