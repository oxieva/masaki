<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 25/12/18
 * Time: 13:05
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Palabrasclave;
use AppBundle\Entity\User;
use AppBundle\Form\TaskType;
use AppBundle\Service\Searcher\NewsSearcher;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class SearchController extends Controller
{
    /**
     * @Route("/search/{page}", name="searcherNoticia", defaults={"page"=1})
     *
     */
    public function SearchResultsPage(RequestStack $requestStack, $page){

        $currentRequest = $requestStack->getCurrentRequest();

        $paramRequest = $currentRequest->query;

        $paramArray = $paramRequest->all();


        /**@var \AppBundle\Service\Searcher\NewsSearcher $newsSearcher*/
        $newsSearcher = $this->get('app.news.searcher');
        list($pager, $noticias) = $newsSearcher->selectNewsBy($paramArray, $page);

        //dump($paramArray); exit();

        return $this->render('AppBundle:full:searchResultsPage.html.twig', [
            'pager' => $pager,
            'noticias' => $noticias,

        ]);


    }

}