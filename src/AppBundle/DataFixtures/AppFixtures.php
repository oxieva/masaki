<?php
namespace AppBundle\DataFixtures;

use AppBundle\Entity\Seccion;
use AppBundle\Entity\Palabrasclave;
use AppBundle\Entity\Noticia;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\Date;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    public function load(ObjectManager $manager)
    {

        $seccionArr = ['Actualitat','Catalunya', 'Política', 'Succesos','Esports', 'Cultura'];

        foreach($seccionArr as $seccioNom){
            $seccion = new Seccion();
            $seccion->setDescripcion($seccioNom);
            $manager->persist($seccion);
            $manager->flush();
        }

        /*$seccion = new Seccion();
        $seccion->setDescripcion('Actualitat');
        $manager->persist($seccion);
        $manager->flush();

        $seccion = new Seccion();
        $seccion->setDescripcion('Catalunya');
        $manager->persist($seccion);
        $manager->flush();

        $seccion = new Seccion();
        $seccion->setDescripcion('Esports');
        $manager->persist($seccion);
        $manager->flush();

        $seccion = new Seccion();
        $seccion->setDescripcion('Cultura');
        $manager->persist($seccion);
        $manager->flush();

        $seccion = new Seccion();
        $seccion->setDescripcion('Opinió');
        $manager->persist($seccion);
        $manager->flush();*/


        $paraulesArr = ['Exclusiva','Última hora', 'Opinió', 'Fi 2018','Entrada 2019', 'Barça'];

        foreach($paraulesArr as $paraulaText){
            $keyword = new Palabrasclave();
            $keyword->setTexto($paraulaText);
            $manager->persist($keyword);
            $manager->flush();
        }

        $user = new User();
        $user->setUsername('eva');
        $user->setEmail('oxiworks@oxieva.com');

        $password = $this->encoder->encodePassword($user, 'pass_1234');
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();

        /* GENERATE NEWS */
        for($i = 0; $i < 35; $i++){
            $noticia = new Noticia();
            $noticia->setTitulo('Título'. $i);
            $noticia->setSubtitulo('Subtítulo'. $i);
            $noticia->setTexto('Text noticia ' . $i);
            $noticia->setAutor($user);
            $noticia->setFecha(new \DateTime('now'));
            $noticia->setSeccion($seccion);
            $noticia->setPublicada(true);
            $manager->persist($noticia);
            $manager->flush();
            echo 'New created: ' . $i . "\n";
        }


        /*for ($i = 0; $i < 10; $i++) {

            $paraula = new Palabrasclave();
            $paraula ->setTexto('Fake news');

            $noticia = new Noticia();
            $noticia->setTitulo('Titol numero ' . $i);
            $noticia->setSubtitulo('Subtitol numero ' . $i);
            $noticia->setSeccion([$seccion]);
            $noticia->setPalabrasClave($paraula);
            $noticia->setFecha(new \DateTime());
            $noticia->setPublicada(1);
            $noticia->setUsername('Eva');
            //$noticia->setAutor('Dimsa');


            $manager->persist($noticia);
        }

        $manager->flush();*/



    }


}