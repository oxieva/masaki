<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Home', ['route' => 'homepage']);

        // access services from the container!
        $em = $this->container->get('doctrine')->getManager();
        // findMostRecent and Blog are just imaginary examples
        $secciones = $em->getRepository('AppBundle:Seccion')->findAll();
        //dump($secciones); exit();

        $menu->addChild('About', [
            'route' => 'showPage',
            'routeParameters' => ['title' => 'about']
            ]
        );

        $menu['About']->addChild('AboutChild', [
                'route' => 'showPage',
                'routeParameters' => ['title' => 'about']]
            );


        // create another menu item
       // $menu->addChild('showPage', ['page' => 'about']);
        // you can also add sub levels to your menus as follows
        // $menu['About Me']->addChild('Edit profile', ['route' => 'edit_profile']);

        // ... add more children

        return $menu;
    }
}