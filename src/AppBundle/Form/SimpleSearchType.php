<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 24/12/18
 * Time: 11:46
 */
namespace AppBundle\Form;


use AppBundle\Entity\Palabrasclave;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SimpleSearchType extends AbstractType
{
    private $entityManager;

    private $router;

    public function __construct(EntityManagerInterface $entityManager, UrlGeneratorInterface $router)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAction($this->router->generate('searcherNoticia'))
            ->setMethod('GET')
            ->add('s', TextType::class, ['label' => 'Paraula a buscar'])
            ->add('search', SubmitType::class, ['label' => 'Search'])
            ->getForm()
        ;
        /** @var \Doctrine\ORM\EntityManager $entityManager */
       // $entityManager = $options['entity_manager'];

    }

    public function configureOptions(OptionsResolver $resolver)
    {
   /*     $resolver->setRequired('entity_manager');*/
    }
}