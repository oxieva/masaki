<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 24/12/18
 * Time: 11:46
 */
namespace AppBundle\Form;


use AppBundle\Entity\Palabrasclave;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityManagerInterface;

class TaskType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextareaType::class)
            ->add('search', SubmitType::class, ['label' => 'Search'])
        ;
        /** @var \Doctrine\ORM\EntityManager $entityManager */
       // $entityManager = $options['entity_manager'];

    }

    public function configureOptions(OptionsResolver $resolver)
    {
   /*     $resolver->setRequired('entity_manager');*/
    }
}