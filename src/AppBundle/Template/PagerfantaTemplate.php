<?php

namespace AppBundle\Template;

use Pagerfanta\View\Template\DefaultTemplate;

class PagerfantaTemplate extends DefaultTemplate
{
    static protected $defaultOptions = array(
        'prev_message'   => '<Anterior',
        'next_message'       => 'Siguiente>',
        'css_disabled_class' => 'disabled',
        'css_dots_class'     => 'dots',
        'css_current_class'  => 'current',
        'dots_text'          => '...',
        'container_template' => '<nav>%pages%</nav>',
        'page_template'      => '<a style="color:%color% !important;" href="%href%"%rel%>%text%</a>',
        'span_template'      => '<span style="color:%color%;" class="%class%"><b>%text%</b></span>',
        'rel_previous'        => 'anterior',
        'rel_next'            => 'siguiente'
    );

    public function container()
    {
        return $this->option('container_template');
    }

    public function page($page)
    {
        $colorMapping = ['1' => '#395067', '2' => '#1bff00','3' => '#395067','4' => '#ff9109'];
        $text = $page ;
        $color = isset($colorMapping[$page]) ? $colorMapping[$page] : '#ff023d';

        return $this->pageWithText($page, $text, null, $color);
    }

    public function pageWithText($page, $text, $rel = null, $color = '#ff023d')
    {
        $search = array('%href%', '%text%', '%rel%', '%color%');

        $href = $this->generateRoute($page);

        $replace = $rel ? array($href, $text, ' rel="' . $rel . '"', $color) : array($href, $text, '',$color);

        return str_replace($search, $replace, $this->option('page_template'));
    }

    public function previousDisabled()
    {
        return $this->generateSpan($this->option('css_disabled_class'), $this->option('prev_message'));
    }

    public function previousEnabled($page)
    {
        return $this->pageWithText($page, $this->option('prev_message'), $this->option('rel_previous'));
    }

    public function nextDisabled()
    {
        return $this->generateSpan($this->option('css_disabled_class'), $this->option('next_message'));
    }

    public function nextEnabled($page)
    {
        return $this->pageWithText($page, $this->option('next_message'), $this->option('rel_next'));
    }

    public function first()
    {
        return $this->page(1);
    }

    public function last($page)
    {
        return $this->page($page);
    }

    public function current($page)
    {
        return $this->generateSpan($this->option('css_current_class'), $page);
    }

    public function separator()
    {
        return $this->generateSpan($this->option('css_dots_class'), $this->option('dots_text'));
    }

    private function generateSpan($class, $page)
    {
        $search = array('%class%', '%text%');
        $replace = array($class, $page);

        return str_replace($search, $replace, $this->option('span_template'));
    }
}